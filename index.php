<?php
//include auth.php file on all secure pages
include("auth.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Welcome Home</title>
<link rel="stylesheet" href="css/style.css" />
</head>
<body>
<div class="form">
<p>Welcome, student <?php echo $_SESSION['id_no']; ?>!</p>
<p>This is secure area.</p>
<button onclick="window.location.href='add_post.php'"> Add Post</button>
<button onclick="window.location.href='view_post.php'"> View Post</button><br><br>
<button onclick="window.location.href='add_event.php'"> Add Event</button>
<button onclick="window.location.href='view_event.php'"> View Event</button><br><br>
<button onclick="window.location.href='add_announcement.php'"> Add Announcement</button>
<button onclick="window.location.href='view_announcement.php'"> View Announcement</button><br>
<!-- <p><a href="add_post.php">Add Post</a></p> -->
<a href="logout.php">Logout</a>
</div>
</body>
</html>